import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path:"/",redirect:"/Login"
    },
    {
        path: '/Login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
    },
    {
        path: '/index',
        name: 'index',
        component: () => import(/* webpackChunkName: "index" */ '../views/index.vue'),
        children:[
            {
                path: "roles",
                name: "roles",
                component: ()=> import(/* webpackChunkName: "Roles" */ '../components/roles/Roles.vue'),
            },
            {
                path: "users",
                name: "users",
                component: ()=> import(/* webpackChunkName: "User" */ '../components/users/User.vue'),
            },
            {
                path: "rights",
                name: "rights",
                component: ()=> import(/* webpackChunkName: "Right" */ '../components/rights/Right.vue'),
            },
            {
                path: "goods",
                name: "goods",
                component: ()=> import(/* webpackChunkName: "goods" */ '../components/goods/goods.vue'),
            },
            {
                path: "orders",
                name: "orders",
                component: ()=> import(/* webpackChunkName: "orders" */ '../components/orders/orders.vue'),
            },
            {
                path: "params",
                name: "params",
                component: ()=> import(/* webpackChunkName: "params" */ '../components/params/params.vue'),
            },
            {
                path: "categories",
                name: "categories",
                component: ()=> import(/* webpackChunkName: "categories" */ '../components/categories/categories.vue'),
            },
            {
                path: "reports",
                name: "reports",
                component: ()=> import(/* webpackChunkName: "reports" */ '../components/reports/reports.vue'),
            }
        ]
    }
]

const router = new VueRouter({
    routes
})

export default router
