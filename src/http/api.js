import { http } from "./index";

export function login(data) {
    return http("login", "POST", data)
}
//获取权限列表
export function getMenus(){
    return http("menus","GET");
}
//获取用户列表
export function getUsers(params){
    return http("users","GET",{},params)
}
//添加用户
export function addUsers(data){
    return http("users",'POST',data)
}
//修改用户
export function reviseUserInfo(id,data){
    return http("users/"+id,"PUT",data)
}
//删除单个用户
export function removeUser(id){
    return http("users/"+id,"DELETE")
}
//获取角色列表
export function getUsersRolesList(){
    return http("roles","GET")
}
//根据ID查询用户信息
export function getUsersID(id){
    return http("users/"+id,"GET")
}
//分配用户权限
export function AssignUserRight(id){
    return http("users/"+id+"/role","PUT")
}