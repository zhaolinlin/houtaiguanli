import axios from "axios";
import { Message } from "element-ui"
import store from "../store"
import router from "../router"


const instance = axios.create({
    baseURL: "http://192.168.1.222:8888/api/private/v1/"
})

//添加请求拦截器
instance.interceptors.request.use(config=>{
    if (store.state.token) {
        config.headers['Authorization'] = store.state.token
    }
    return config
},(error)=>{
    return Promise.reject(error)
})
//响应拦截器

instance.interceptors.response.use(response=>{
    if (response.data.meta.msg === '无效token') {
        router.push({
            name: 'Login'
        })
    }
    return response
},error=>{
    return Promise.reject(error)
})


export function http(url, method, data, params) {
    return new Promise((resolve, reject) => {
        instance({
            url,
            method,
            data,
            params
        }).then(res => {
            if (res.status >= 200 && res.status < 300 || res.status === 304) {
                if (res.data.meta.status >= 200 && res.data.meta.status < 300 || res.data.meta.status === 304) {
                    resolve(res.data)
                } else {
                    Message({
                        showClose: true,
                        message: res.data.meta.msg,
                        type: "error"
                    })
                    reject(res)
                }
            } else {
                Message({
                    showClose: true,
                    message: res.statusText,
                    type: "error"
                })
                reject(res.data.data)
            }
        }).catch(err => {
            Message({
                showClose: true,
                message: err.message,
                type: "error"
            })
            reject(err)
        })
    })
}